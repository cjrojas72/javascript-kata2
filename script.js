/////////////////////// add function ///////////////////////////////////////

function add(num1, num2) {
  let total = num1 + num2;

  return total;
}

console.log(add(2, 3));

function subtract(num1, num2) {
  let total = num1 - num2;

  return total;
}

console.log(subtract(5, 2));
///////////////// multiply function //////////////////////////////////////

function multi(num1, num2) {
  let total = num1;
  for (let i = num2; i > 1; i--) {
    total = add(total, num1);
  }

  return total;
}

console.log(multi(3, 9));

////////////// power function ////////////////////////////////////////

function power(num1, num2) {
  if (num1 > 0 && num2 === 0) {
    return 1;
  }

  pow = num1;

  while (num2 > 1) {
    pow = multi(num1, pow);
    num2 = subtract(num2, 1);
  }
  return pow;
}

console.log(power(3, 4));

///////////////// factorial  ////////////////////////////////////////////

function factorial(num) {
  if (num === 0) {
    return 1;
  }
  return multi(num, factorial(subtract(num, 1)));
}

console.log(factorial(4));

/////////////// fibonacci ////////////////////////////////////////////

function fibonacci(num) {
  if (num <= 1) return num;

  return add(fibonacci(subtract(num, 1)), fibonacci(subtract(num, 2)));
}

console.log(fibonacci(7));
